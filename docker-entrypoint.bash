#!/bin/bash
set -e

if [[ "$1" == "bitcoin-cli" \
   || "$1" == "bitcoin-tx" \
   || "$1" == "bitcoind" \
   || "$1" == "test_bitcoin" ]]; then
   mkdir -p "$bitcoin_d"

   if [[ ! -s "$bitcoin_d/bitcoin.conf" ]]; then
      	 cat <<-EOF > "$bitcoin_d/bitcoin.conf"
printtoconsole=1
rpcallowip=::/0
rpcpassword=${BITCOIN_RPC_PASSWORD:-password}
rpcuser=${BITCOIN_RPC_USER:-bitcoin}
EOF
	chown bitcoin:bitcoin "$BITCOIN_DATA/bitcoin.conf"
												      fi

												      # ensure correct ownership and linking of data directory
												      	# we do not update group ownership here, in case users want to mount
													  # a host directory and still retain access to it
  chown -R bitcoin "$bitcoin_d"
  ln -sfn "$bitcoin_d" /home/bitcoin/.bitcoin
  chown -h bitcoin:bitcoin /home/bitcoin/.bitcoin
  exec gosu bitcoin "$@"

fi
exec "$@"