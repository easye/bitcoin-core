FROM debian:stretch

RUN groupadd -r bitcoin && useradd -r -m -g bitcoin bitcoin
RUN set -ex \
    && apt-get update \
    && apt-get install -qq --no-install-recommends ca-certificates dirmngr gosu gpg wget \
    && rm -rf /var/lib/apt/lists/*

ENV bitcoin_version 0.14.2
ENV bitcoin_uri https://bitcoin.org/bin/bitcoin-core-${bitcoin_version}/bitcoin-${bitcoin_version}-x86_64-linux-gnu.tar.gz
ENV bitcoin_sha256 20ACC6D5D5E0C4140387BC3445B8B3244D74C1C509BD98F62B4EE63BEC31A92B
ENV bitcoin_asc_uri https://bitcoin.org/bin/bitcoin-core-${bitcoin_version}/SHA256SUMS.asc
ENV bitcoin_pgp_key 01EA5486DE18A882D4C2684590C8019E36C2E964

# install bitcoin binaries
RUN set -ex \
 && dist=xx \
 && wget -O $dist $bitcoin_uri \
 && echo "$bitcoin_sha256 $dist" | sha256sum -c - \
 && gpg --keyserver keyserver.ubuntu.com --recv-keys $bitcoin_pgp_key \
 && wget -O bitcoin.asc $bitcoin_asc_uri \
 && gpg --verify bitcoin.asc \
 && mkdir -p /usr/local \
 && tar -xzv -f $dist -C /usr/local --strip-components=1 --exclude=*-qt \
 && rm $dist bitcoin.asc

# create data directory
ENV bitcoin_d /var/local/bitcoin.d
RUN set -ex \
  && mkdir -p ${bitcoin_d} \
  && chown -R bitcoin:bitcoin $bitcoin_d \
  && ln -sfn  $bitcoin_d /home/bitcoin/.bitcoin \
  && chown -h bitcoin:bitcoin /home/bitcoin/.bitcoin 

VOLUME /var/local/bitcoin.d


COPY index.n3	/index.n3
COPY docker-entrypoint.bash /entrypoint.bash
RUN chmod +x /entrypoint.bash
ENTRYPOINT ["/entrypoint.bash"]

EXPOSE 8332 8333 18332 18333
CMD ["bitcoind"]